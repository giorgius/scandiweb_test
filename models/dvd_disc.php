<?php

include('tools/autoload.php');

class dvd_disc extends Product{
    public $size;
    private $typeId;

    function __construct($sku,$name,$price,$size){
        $this->size=$size;
        parent::__construct($sku,$name,$price);
        $this->typeId = TypeIds::getTypeIdByName(get_class($this));
    }

    function getSize(){
        return $this->size;
    }

    function setSize($s){
        $this->size=$s;
    }

    public function isValid(){
        $validator = new Validator();

        return $validator->isValid($this);
    }

    public function save(){
        $conn = Db::connect();

        if (!mysqli_query($conn,"insert into product (sku,name,price,type,size) values ('$this->sku','$this->name','$this->price','$this->typeId','$this->size')"))
        {
                
                Alert::customAlert('error', "Error code: ".mysqli_errno($conn));

                $conn->close();
                return false;
        }

        $conn->close();
        return true;
    }

    

}

?>