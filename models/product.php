<?php

class Product{

    public $sku;
    public $name;
    public $price;

    function __construct($s,$n,$p){
        $this->sku=$s;
        $this->name=$n;
        $this->price=$p;
    }

    function getsku(){
        return $this->sku;
    }

    function setsku($s){
        $this->sku=$s;
    }

    function getName(){
        return $this->name;
    }

    function setName($n){
        $this->name=$n;
    }
}

?>


