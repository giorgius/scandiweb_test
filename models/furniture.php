<?php

include('tools/autoload.php');

class Furniture extends Product{
        public $width;
        public $height;
        public $length;
        private $typeId;

        function __construct($sku,$name,$price,$width,$height,$length){
            $this->length=$length;
            $this->height=$height;
            $this->width=$width;
            parent::__construct($sku,$name,$price);
            $this->typeId = TypeIds::getTypeIdByName(get_class($this));
        }

        public function isValid(){
            $validator = new Validator();
    
            return $validator->isValid($this);
        }
    
        public function save(){
            $conn = Db::connect();

            if (!mysqli_query($conn,"insert into product (sku,name,price,type,width,height,length) values ('$this->sku','$this->name','$this->price','$this->typeId','$this->width','$this->height','$this->length')"))
            {
                
                Alert::customAlert('error', "Error code: ".mysqli_errno($conn));

                $conn->close();
                return false;
            }
           
            $conn->close();
            return true;
        }

        function getWidth(){
            return $this->width;
        }

        function setWidth($w){
            $this->width = $w;
        }

        function getHeight(){
            return $this->height;
        }

        function setHeight($h){
            $this->height = $h;
        }

        function getLength(){
            return $this->length;
        }

        function setLength($l){
            $this->length = $l;
        }
}

?>