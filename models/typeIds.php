<?php


class TypeIds{
    public static $types = array("dvd_disc" => 1,"Book" => 2,"Furniture" => 3);
    
    public static function getTypeIdByName($name){
        return self::$types[$name];
    } 
}

?>