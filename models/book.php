<?php

include('tools/autoload.php');

class Book extends Product{
    public $weight;
    private $typeId; 

    function __construct($sku,$name,$price,$weight){
        $this->weight=$weight;
        parent::__construct($sku,$name,$price);
        $this->typeId = TypeIds::getTypeIdByName(get_class($this));
    }

    function getWeight(){
        return $this->weight;
    }

    function setWeight($w){
        $this->weight=$w;
    }

    public function isValid(){
        $validator = new Validator();

        return $validator->isValid($this);
    }

    public function save(){
        $conn = Db::connect();

        if (!mysqli_query($conn,"insert into product (sku,name,price,type,weight) values ('$this->sku','$this->name','$this->price','$this->typeId','$this->weight')"))
        {
                
                Alert::customAlert('error', "Error code: ".mysqli_errno($conn));

                $conn->close();
                return false;
        }

        $conn->close();
        return true;
    }
    
}

?>

