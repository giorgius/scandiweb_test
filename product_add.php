
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/site.css"> 
    <title>Product New</title>
</head>
<body>
    <header>

    </header>
    <form action="" method="post">
        <div class="top-content">
            <h2> Product Add </h2>
            <input type="submit" name="submit" value="Save" />
        </div>
        <div class="content">
            <span id="top-message" class="">
                
            </span>
            <ul>
                <li> SKU <input type="text" name="sku" /> </li>
                <li> Name <input type="text" name="name" /> </li>
                <li> Price <input type="text" name="price" /> </li>
                <li> Type 
                    <select name="type" id="type-select"> 
                        <option value="0" selected> Select type </option>
                        <option value="1"> DVD-disc </option>
                        <option value="2"> Book </option>
                        <option value="3"> Furniture </option>
                        
                    </select>
            </li>
            <li id="type-props">
           
            </li>
            </ul>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script>
        //load header
        $("header" ).load( "partial/header.html"); 

        //set selected index of type switcher to 0 when reload
        let select = document.getElementById('type-select');
        select.selectedIndex = 0;

        //load right fields on type switcher value change
        select.onchange = (e) => {
            switch(e.target.selectedIndex){
                case 1:
                    $("#type-props" ).load( "partial/types/dvd_disc.html"); 
                break;
                case 2:
                    $("#type-props" ).load( "partial/types/book.html"); 
                break;
                case 3:
                    $("#type-props" ).load( "partial/types/furniture.html"); 
                break;
                default:
                    $("#type-props" ).empty();
                break;
            }
            
        };

    </script>
</body>
</html>

<?php
    require_once('tools/autoload.php');

    //handle post request, detect product type and if object is valid then save it to the database
    if(isset($_POST['submit'])){
            $sku = $_POST['sku'];
            $name = $_POST['name'];
            $price = $_POST['price'];

        switch($_POST['type']){
            case 1:
                $size = $_POST['size'];
                $disk = new dvd_disc($sku,$name,$price,$size);
                if($disk->isValid()){
                    if($disk->save()){
                        Alert::customAlert('success','Product saved successfully.');
                    };
                   
                }
                else{
                    Alert::customAlert('error','Wrong input format.');
                }
                
            break;
            case 2:
                $weight = $_POST['weight'];
                $book = new Book($sku,$name,$price,$weight);
                if($book->isValid()){
                    if($book->save()){
                        Alert::customAlert('success','Product saved successfully.');

                    };   
                }
                else{
                    Alert::customAlert('error','Wrong input format.');
                }
    
            break;
            case 3:
                $width = $_POST['width'];
                $height = $_POST['height'];
                $length = $_POST['length'];
                $furniture = new Furniture($sku,$name,$price,$width,$height,$length);
                if($furniture->isValid()){
                    if($furniture->save()){
                        Alert::customAlert('success','Product saved successfully.');
                    };
                }
                else{
                    Alert::customAlert('error','Wrong input format.');
                }
    
            break;
        }

    }
?>

