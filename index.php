<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/site.css"> 
    <title>Product List</title>
</head>
<body>
    
        <header>

        </header>
    <form action="" method="post">
        <div class="top-content">
            <h2> Product List </h2>
           
                <input type="submit" name="delete" id="applyBtn" value="Apply" />
           
            <select style="margin-right:10px;margin-top:15px;">
                <option> Mass delete action </option>
            </select>
        </div>
        <div class="content">
            <span id="top-message" class="">
                
            </span>
            <?php

                include('tools/autoload.php');

                $conn = Db::connect();

                $sql = "SELECT * FROM product";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
         
                    while($row = $result->fetch_assoc()) {
                        echo "<div onclick='itemClicked(this)' class=\"item-container\">
                                <input type=\"checkbox\" name=\"productIds[]\" class=\"itemCheckbox\" value=\"" . $row["id"] . "\" />
                                <span class=\"itemDetailsText\">" . $row["sku"] . "</span>
                                <span class=\"itemDetailsText\">" . $row["name"] .  "</span>
                                <span class=\"itemDetailsText\">" . $row["price"] .  " $</span>
                                <span class=\"itemDetailsText\">" .(($row["type"]==1)?'Size: '.$row["size"].' MB':(($row["type"]==2)?'Weight: '.$row["weight"].' KG':'Dimension: '.$row["height"].'x'.$row["width"].'x'.$row["length"])). "</span>
                            </div>";
                    }
                } else {
                    echo "<div class=\"noItemsText\"> No products to show. </div>";
                }
                $conn->close();


                

            ?>
            
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
   <script>
        //load header
        $("header" ).load( "partial/header.html"); 

        //toggle chechbox checked value when product clicked
       let itemClicked = (e) =>{
          
            if(e.childNodes[1].checked){
                e.childNodes[1].checked = false;
            }
            else{
                e.childNodes[1].checked = true;
            };
       }


    </script>
</body>
</html>

<?php
        //mass delete action
        if(isset($_POST['delete'])){

            if(isset($_POST['productIds'])){

                $idsString = "";

                //make string from selected product ids for example "1,3,4,3"
                foreach($_POST['productIds'] as $id){
                    $idsString = $idsString.$id.",";
                }

                $conn = Db::connect();

                $sql = "DELETE FROM product WHERE id IN (".substr($idsString, 0, -1).")";

                //check if query succeeded and if yes refresh page to see changes
                if (mysqli_query($conn, $sql)) {
                    header("Refresh:0");
                } else {
                    Alert::customAlert('error','Oops something went wrong.');
                }

                $conn->close();
            };
            
 
        }
?>