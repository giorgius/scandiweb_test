<?php

class Validator{

    public function isValid($object){
        $isValid = true;

        if(!$object->sku){
            $isValid = false;
        }

        if(!$object->name){
            $isValid = false;
        }

        if(!is_numeric($object->price) || !$object->price){
            $isValid = false;
        }

        switch(get_class($object)){
            case 'dvd_disc':
                if(!is_numeric($object->size) || !$object->size){
                    $isValid = false;
                }
            break;
            case 'Book':
                if(!is_numeric($object->weight) || !$object->weight){
                    $isValid = false;
                }
            break;
            case 'Furniture':
                
                if(!is_numeric($object->width) || !$object->width){
                    $isValid = false;
                }

                if(!is_numeric($object->height) || !$object->height){
                    $isValid = false;
                }
                if(!is_numeric($object->length) || !$object->length){
                    $isValid = false;
                }
            break;
            default:
                $isValid = false;
            break;
        }

        return $isValid;
    }

}


?>